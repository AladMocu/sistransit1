package uniandes.isis2304.superAndes.persistencia;
import uniandes.isis2304.superAndes.negocio.eCategoria;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.sql.Date;

public class SQLProducto {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;

    public SQLProducto(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }

    public long adicionarProducto(PersistenceManager pm,long id,float cantidad,String codigoBarras,Date vencimiento,String marca,String nombre,float pesoEmpaque,String presentacion,float preciounidaddemedida,float preciounitario,String tipoproducto,String unidadedida,float volumenempaque)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaProducto () + "\" (\"ProductoID\",\"Cantidadproducto\",\"Codigodebarras\",\"Fechavencimiento\",\"Marca\",\"Nombre\",\"Pesoempaque\",\"Precentacion\",\"Precioporunidaddemedida\",\"Preciounitario\",\"Tipoproducto\",\"Unidadmedida\",\"Volumenempaque\") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        q.setParameters(id, cantidad, codigoBarras,vencimiento,marca,nombre, pesoEmpaque,presentacion,preciounidaddemedida,preciounitario,tipoproducto, unidadedida,volumenempaque);
        return (long) q.executeUnique();
    }
    public long eliminarProducto (PersistenceManager pm, long idProducto)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaProducto () + "\" WHERE \"ProductoID\" = ?");
        q.setParameters(idProducto);
        return (long) q.executeUnique();
    }

}