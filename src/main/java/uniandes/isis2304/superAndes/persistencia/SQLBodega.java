package uniandes.isis2304.superAndes.persistencia;

import uniandes.isis2304.superAndes.negocio.Bodega;
import uniandes.isis2304.superAndes.negocio.SuperAndes;
import uniandes.isis2304.superAndes.negocio.eCategoria;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.List;

public class SQLBodega {
    private final static String SQL = PersistenciaSuperAndes.SQL;

    private PersistenciaSuperAndes pp;

    public SQLBodega(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }

    public long adicionarBodega (PersistenceManager pm, long idBodega, double capacidadPeso, double capacidadVolumen,String nivelReorden, double pesoDisponible,eCategoria tipoAlmacenamiento,double volumendisponible,long sucursalid)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaBodega () + "\" (\"BodegaID\",\"Capacidadpeso\",\"Capacidadvolumen\",\"Nivelreorden\",\"Pesodisponible\",\"Tipoalmacenamiento\",\"Volumendisponible\",\"SucursalID\") values (?, ?, ?, ?, ?,?,?,?)");
        q.setParameters(idBodega,capacidadPeso,capacidadVolumen,nivelReorden,pesoDisponible,tipoAlmacenamiento,volumendisponible,sucursalid);
        return (long) q.executeUnique();
    }

    public long eliminarBodega(PersistenceManager pm, long BodegaID)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaBodega () + "\" WHERE \"BodegaID\" = ?");
        q.setParameters(BodegaID);
        return (long) q.executeUnique();
    }

    public List<Bodega> listar(PersistenceManager pm)
    {
        Query q = pm.newQuery(SQL, "SELECT * FROM \"" + pp.darTablaBodega()+"\"");
        q.setResultClass(Bodega.class);
        return (List<Bodega>) q.executeList();
    }

}
