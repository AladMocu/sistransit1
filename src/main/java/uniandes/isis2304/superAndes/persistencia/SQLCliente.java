package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLCliente {
    private final static String SQL = PersistenciaSuperAndes.SQL;

    private PersistenciaSuperAndes pp;

    public SQLCliente(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }


    public long adicionarCliente (PersistenceManager pm, long idCliente, String email, String Nombre)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaCliente () + "\" (\"ClienteID\", \"Email\", \"Nombre\") values (?, ?, ?)");
        q.setParameters(idCliente,email,Nombre);
        return (long)q.executeUnique();
    }

    public long eliminarCliente (PersistenceManager pm, long idCliente)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaCliente () + "\" WHERE \"ClienteID\" = ?");
        q.setParameters(idCliente);
        return (long) q.executeUnique();
    }


}
