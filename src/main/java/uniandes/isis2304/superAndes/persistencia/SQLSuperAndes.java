package uniandes.isis2304.superAndes.persistencia;

import uniandes.isis2304.superAndes.negocio.SuperAndes;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.ArrayList;
import java.util.List;

public class SQLSuperAndes {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;
    public SQLSuperAndes(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }

    public long adicionarSupermercado(PersistenceManager pm, long SupermercadoID, String Nombre)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \""+ pp.darTablaSuperAndes() +"\"(\"SuperandesID\",\"Nombre\") values ( ? ,? )");
        q.setParameters(SupermercadoID,Nombre);
        return (long) q.executeUnique();
    }
    public long eliminarSupermercado (PersistenceManager pm, long SupermercadoID)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaSuperAndes() + "\" WHERE \"SuperandesID\" = ?");
        q.setParameters(SupermercadoID);
        return (long) q.executeUnique();
    }

    public List<SuperAndes> listar(PersistenceManager pm)
    {
        Query q = pm.newQuery(SQL, "SELECT * FROM \"" + pp.darTablaSuperAndes()+"\"");
        q.setResultClass(SuperAndes.class);
        return (List<SuperAndes>) q.executeList();
    }
}
