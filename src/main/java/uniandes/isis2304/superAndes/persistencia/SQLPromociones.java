package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.sql.Date;

public class SQLPromociones {

    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;

    public SQLPromociones(PersistenciaSuperAndes pp) {
        this.pp = pp;
    }

    public long adicionarPromocion(PersistenceManager pm, long promocionID,long ProductoPromocion,int tipoPromocion,String descripcionPromocion,Date finicio,Date fFinal) {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaPromociones() + "\"(\"PromocionID\", \"ProductoPromocion\", \"TipoPromocion\",\"DescripcionPromocion\",\"FechaInicio\",\"FechaFin\") values (?, ?, ? ,? ,?,?,?)");
        q.setParameters(promocionID,ProductoPromocion,tipoPromocion,descripcionPromocion,finicio,fFinal);
        return (long) q.executeUnique();
    }

    public long eliminarPromocion(PersistenceManager pm, long PromocionID) {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaPromociones() + "\" WHERE \"PromocionID\" = ?");
        q.setParameters(PromocionID);
        return (long) q.executeUnique();
    }
}
