package uniandes.isis2304.superAndes.persistencia;

import uniandes.isis2304.superAndes.negocio.Estante;
import uniandes.isis2304.superAndes.negocio.eCategoria;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.List;

public class SQLEstante {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;

    public SQLEstante(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }

    public long adicionarEstante(PersistenceManager pm,long EstanteID,float nivelavastecimiento,eCategoria tipoestante,long SucursalID)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaEstante () + "\" (\"EstanteID\",\" Nivelavastecimiento\", \"Tipoestante\", \"SucursalID\") values (?, ?, ?, ?)");
        q.setParameters(EstanteID,nivelavastecimiento,tipoestante,SucursalID);
        return (long) q.executeUnique();
    }
    public long eliminarEstante (PersistenceManager pm, long idEstante)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaEstante () + "\" WHERE \"EstanteID\" = ?");
        q.setParameters(idEstante);
        return (long) q.executeUnique();
    }
    public List<Estante> listar(PersistenceManager pm)
    {
        Query q = pm.newQuery(SQL, "SELECT * FROM \"" + pp.darTablaEstante()+"\"");
        q.setResultClass(Estante.class);
        return (List<Estante>) q.executeList();
    }

}
