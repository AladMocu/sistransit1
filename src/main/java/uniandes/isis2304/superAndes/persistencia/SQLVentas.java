package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.sql.Date;

public class SQLVentas {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;

    public SQLVentas(PersistenciaSuperAndes pp) {
        this.pp = pp;
    }

    public long adicionarVenta(PersistenceManager pm, long promocionID, long ProductoPromocion, int tipoPromocion, String descripcionPromocion, Date finicio, Date fFinal) {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaPromociones() + "\" (\"PromocionID\", \"ProductoPromocion\", \"TipoPromocion\" ,\"DescripcionPromocion\",\"FechaInicio\",\"FechaFin\") values (?, ?, ? ,? ,?,?,?)");
        q.setParameters(promocionID,ProductoPromocion,tipoPromocion,descripcionPromocion,finicio,fFinal);
        return (long) q.executeUnique();
    }

    public long eliminarVenta(PersistenceManager pm, long VentaID) {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaPromociones() + "\" WHERE \"VentaID\" = ?");
        q.setParameters(VentaID);
        return (long) q.executeUnique();
    }
}
