package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLProductoSucursal {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;
    public SQLProductoSucursal(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }

    public long adicionarProductoSucursal(PersistenceManager pm, long PSID, float PrecioventasPublico,long BodegaID,long EstanteID)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaProductoSucursal() + "\" (\"ProducosucursalID\",\"Precioventapublico\",\"BodegaID\",\"EstanteID\") values (?, ?,?,?)");
        q.setParameters(PSID,PrecioventasPublico,BodegaID,EstanteID);
        return (long) q.executeUnique();
    }
    public long eliminarProductoSucursal (PersistenceManager pm, long idPS)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaProductoSucursal () + "\" WHERE \"ProducosucursalID\" = ?");
        q.setParameters(idPS);
        return (long) q.executeUnique();
    }


}
