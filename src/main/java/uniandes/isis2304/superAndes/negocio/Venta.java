package uniandes.isis2304.superAndes.negocio;

import java.util.ArrayList;
import java.util.Date;

public class Venta {

    private long VENTAID;

    private long PRODUCTO;

    private  float CANTIDAD;

    private long PUNTOVENTA;

    private long PROMOCION;

    private long CLIENTE;

    public Venta()
    {

    }


    public Venta(long VENTAID, long PRODUCTO, float CANTIDAD, long PUNTOVENTA, long PROMOCION, long CLIENTE) {
        this.VENTAID = VENTAID;
        this.PRODUCTO = PRODUCTO;
        this.CANTIDAD = CANTIDAD;
        this.PUNTOVENTA = PUNTOVENTA;
        this.PROMOCION = PROMOCION;
        this.CLIENTE = CLIENTE;
    }

    public long getVENTAID() {
        return VENTAID;
    }

    public void setVENTAID(long VENTAID) {
        this.VENTAID = VENTAID;
    }

    public long getPRODUCTO() {
        return PRODUCTO;
    }

    public void setPRODUCTO(long PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }

    public float getCANTIDAD() {
        return CANTIDAD;
    }

    public void setCANTIDAD(float CANTIDAD) {
        this.CANTIDAD = CANTIDAD;
    }

    public long getPUNTOVENTA() {
        return PUNTOVENTA;
    }

    public void setPUNTOVENTA(long PUNTOVENTA) {
        this.PUNTOVENTA = PUNTOVENTA;
    }

    public long getPROMOCION() {
        return PROMOCION;
    }

    public void setPROMOCION(long PROMOCION) {
        this.PROMOCION = PROMOCION;
    }

    public long getCLIENTE() {
        return CLIENTE;
    }

    public void setCLIENTE(long CLIENTE) {
        this.CLIENTE = CLIENTE;
    }
}
