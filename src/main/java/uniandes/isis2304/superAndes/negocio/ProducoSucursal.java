package uniandes.isis2304.superAndes.negocio;

/**
 * Clase que describe el producto de la sucursal
 * se compone por el precio del producto
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:19 p. m.
 */
public class ProducoSucursal extends Producto  {

	private long  ProductosucursalID;
	private float Precioventapublico;
	private long BodegaID;
	private long EstanteID;

	public ProducoSucursal()
	{

	}

	public ProducoSucursal(long productosucursalID, float precioventapublico, long bodegaID, long estanteID) {
		ProductosucursalID = productosucursalID;
		Precioventapublico = precioventapublico;
		BodegaID = bodegaID;
		EstanteID = estanteID;
	}


	public long getProductosucursalID() {
		return ProductosucursalID;
	}

	public void setProductosucursalID(long productosucursalID) {
		ProductosucursalID = productosucursalID;
	}

	public float getPrecioventapublico() {
		return Precioventapublico;
	}

	public void setPrecioventapublico(float precioventapublico) {
		Precioventapublico = precioventapublico;
	}

	public long getBodegaID() {
		return BodegaID;
	}

	public void setBodegaID(long bodegaID) {
		BodegaID = bodegaID;
	}

	public long getEstanteID() {
		return EstanteID;
	}

	public void setEstanteID(long estanteID) {
		EstanteID = estanteID;
	}
}//end Produco Sucursal