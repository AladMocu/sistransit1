package uniandes.isis2304.superAndes.negocio;

import java.util.ArrayList;

/**
 * Clase que representa la bodega de las bebidas
 * Maneja los productos almacenados en la sucursal
 * Es responsable del control de la cantidad de los productos en cada sucursal
 * se modela con las características de la bodega
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:17 p. m.
 */

public class Bodega  {

	private double Capacidadpeso;
	private double Capacidadvolumen;
	private String Nivelreorden;

	public Bodega()
	{

	}

    public Bodega(double capacidadpeso, double capacidadvolumen, String nivelreorden, double pesodisponible, String tipoalmacenamiento, double volumendisponible, long sucursalID) {
        Capacidadpeso = capacidadpeso;
        Capacidadvolumen = capacidadvolumen;
        Nivelreorden = nivelreorden;
        Pesodisponible = pesodisponible;
        Tipoalmacenamiento = tipoalmacenamiento;
        Volumendisponible = volumendisponible;
        SucursalID = sucursalID;
    }

    public double getCapacidadpeso() {
		return Capacidadpeso;
	}

	public void setCapacidadpeso(double capacidadpeso) {
		Capacidadpeso = capacidadpeso;
	}

	public double getCapacidadvolumen() {
		return Capacidadvolumen;
	}

	public void setCapacidadvolumen(double capacidadvolumen) {
		Capacidadvolumen = capacidadvolumen;
	}

	public String getNivelreorden() {
		return Nivelreorden;
	}

	public void setNivelreorden(String nivelreorden) {
		Nivelreorden = nivelreorden;
	}

	public double getPesodisponible() {
		return Pesodisponible;
	}

	public void setPesodisponible(double pesodisponible) {
		Pesodisponible = pesodisponible;
	}

	public String getTipoalmacenamiento() {
		return Tipoalmacenamiento;
	}

	public void setTipoalmacenamiento(String tipoalmacenamiento) {
		Tipoalmacenamiento = tipoalmacenamiento;
	}

	public double getVolumendisponible() {
		return Volumendisponible;
	}

	public void setVolumendisponible(double volumendisponible) {
		Volumendisponible = volumendisponible;
	}

	public long getSucursalID() {
		return SucursalID;
	}

	public void setSucursalID(long sucursalID) {
		SucursalID = sucursalID;
	}

	public long getBodegaID() {
		return BodegaID;
	}

	public void setBodegaID(long bodegaID) {
		BodegaID = bodegaID;
	}

	private double Pesodisponible;
	private String Tipoalmacenamiento;
	private double Volumendisponible;
	private long SucursalID;
	private long BodegaID;

}//end Bodega