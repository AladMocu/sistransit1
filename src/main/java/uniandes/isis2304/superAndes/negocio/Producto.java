package  uniandes.isis2304.superAndes.negocio;

import oracle.sql.DATE;

import java.sql.Date;
import java.util.*;
/**
 * Clase que describe el producto
 * esta comformada por los elementos del producto
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:23 p. m.
 */


public class Producto {
	private long ProductoID;

	private float Catidadproducto;

	private String Codigodebarras;

	private Date Fechavencimiento;

	private String Marca;

	private String Nombre;

	private float Pesoempaque;

	private  String Precentacion;

	private float Precioporunidaddemedida;

	private float Preciounitario;

	private String Tipoproducto;

	private String Unidadmedida;

	private float Volumenempaque;

	public Producto()
    {

    }

    public Producto(long productoID, float catidadproducto, String codigodebarras, Date fechavencimiento, String marca, String nombre, float pesoempaque, String precentacion, float precioporunidaddemedida, float preciounitario, String tipoproducto, String unidadmedida, float volumenempaque) {
        ProductoID = productoID;
        Catidadproducto = catidadproducto;
        Codigodebarras = codigodebarras;
        Fechavencimiento = fechavencimiento;
        Marca = marca;
        Nombre = nombre;
        Pesoempaque = pesoempaque;
        Precentacion = precentacion;
        Precioporunidaddemedida = precioporunidaddemedida;
        Preciounitario = preciounitario;
        Tipoproducto = tipoproducto;
        Unidadmedida = unidadmedida;
        Volumenempaque = volumenempaque;
    }

    public long getProductoID() {
        return ProductoID;
    }

    public void setProductoID(long productoID) {
        ProductoID = productoID;
    }

    public float getCatidadproducto() {
        return Catidadproducto;
    }

    public void setCatidadproducto(float catidadproducto) {
        Catidadproducto = catidadproducto;
    }

    public String getCodigodebarras() {
        return Codigodebarras;
    }

    public void setCodigodebarras(String codigodebarras) {
        Codigodebarras = codigodebarras;
    }

    public Date getFechavencimiento() {
        return Fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        Fechavencimiento = fechavencimiento;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        Marca = marca;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public float getPesoempaque() {
        return Pesoempaque;
    }

    public void setPesoempaque(float pesoempaque) {
        Pesoempaque = pesoempaque;
    }

    public String getPrecentacion() {
        return Precentacion;
    }

    public void setPrecentacion(String precentacion) {
        Precentacion = precentacion;
    }

    public float getPrecioporunidaddemedida() {
        return Precioporunidaddemedida;
    }

    public void setPrecioporunidaddemedida(float precioporunidaddemedida) {
        Precioporunidaddemedida = precioporunidaddemedida;
    }

    public float getPreciounitario() {
        return Preciounitario;
    }

    public void setPreciounitario(float preciounitario) {
        Preciounitario = preciounitario;
    }

    public String getTipoproducto() {
        return Tipoproducto;
    }

    public void setTipoproducto(String tipoproducto) {
        Tipoproducto = tipoproducto;
    }

    public String getUnidadmedida() {
        return Unidadmedida;
    }

    public void setUnidadmedida(String unidadmedida) {
        Unidadmedida = unidadmedida;
    }

    public float getVolumenempaque() {
        return Volumenempaque;
    }

    public void setVolumenempaque(float volumenempaque) {
        Volumenempaque = volumenempaque;
    }
}//end Producto