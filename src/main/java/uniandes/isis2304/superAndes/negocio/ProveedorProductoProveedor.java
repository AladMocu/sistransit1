package uniandes.isis2304.superAndes.negocio;

public class ProveedorProductoProveedor {
    private long ProveedorID;

    private long ProductoProveedorID;

    public  ProveedorProductoProveedor()
    {

    }

    public ProveedorProductoProveedor(long proveedorID, long productoProveedorID) {
        ProveedorID = proveedorID;
        ProductoProveedorID = productoProveedorID;
    }

    public long getProveedorID() {
        return ProveedorID;
    }

    public void setProveedorID(long proveedorID) {
        ProveedorID = proveedorID;
    }

    public long getProductoProveedorID() {
        return ProductoProveedorID;
    }

    public void setProductoProveedorID(long productoProveedorID) {
        ProductoProveedorID = productoProveedorID;
    }
}
