package uniandes.isis2304.superAndes.negocio;


/**
 * Clase que describe la empresa
 * tiene una dirección y un NIT unico
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:13 p. m.
 */
public class Empresa extends Cliente {
	private long EmpresaID;
	private String Direccion;
	private String Nit;


	public Empresa(long empresaID, String direccion, String nit) {
		EmpresaID = empresaID;
		Direccion = direccion;
		Nit = nit;
	}

	public Empresa()
	{

	}
}//end Empresa