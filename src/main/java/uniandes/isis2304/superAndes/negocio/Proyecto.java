package uniandes.isis2304.superAndes.negocio;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import com.google.gson.JsonObject;
import uniandes.isis2304.superAndes.*;
import uniandes.isis2304.superAndes.persistencia.PersistenciaSuperAndes;

public class Proyecto {
    private static Logger log = Logger.getLogger(SuperAndes.class.getName());


    private PersistenciaSuperAndes pp;



    public Proyecto() {
        pp = PersistenciaSuperAndes.getInstance();

    }

    public Proyecto(JsonObject tableConfig) {
        pp = PersistenciaSuperAndes.getInstance(tableConfig);
    }


    public SuperAndes adicionarSupermercado(String nombre)
    {
        log.info("Adicionando supermercado: "+nombre);
        SuperAndes superAndes= pp.adicionarSupermercado(nombre);
        log.info("Adicionado supermercado: "+superAndes);
        return  superAndes;
    }



    public List<SuperAndes> listarSupermercados()
    {
        List<SuperAndes> rta = new LinkedList<>();
        for(SuperAndes sa:pp.listarSupermercados())
        {
            rta.add(sa);
        }

        return rta;
    }

    public List<Proveedor> listarProveedores()
    {
        List<Proveedor> rta= new LinkedList<>();
        List a= pp.listarProveedores();
        System.out.println(a.size()+" ;;; "+a.toArray());
        System.out.println("PRE");
        for (Proveedor p: pp.listarProveedores()) {
            rta.add(p);
        }
        System.out.println("POST");
        return rta;
    }
    public List<Bodega> listarBodegas()
    {
        List<Bodega> rta= new LinkedList<>();
        List a= pp.listarProveedores();
        System.out.println(a.size()+" ;;; "+a.toArray());
        System.out.println("PRE");
        for (Bodega p: pp.listarBodegas()) {
            rta.add(p);
        }
        return rta;
    }
    public List<Estante> listarEstantes()
    {
        List<Estante> rta= new LinkedList<>();
        List a= pp.listarEstantes();
        System.out.println(a.size()+" ;;; "+a.toArray());
        System.out.println("PRE");
        for (Estante p: pp.listarEstantes()) {
            rta.add(p);
        }
        System.out.println("POST");
        return rta;
    }


    public Proveedor adicionarProveedor(String nombre, String nit, Date fechaentrega, long id)
    {
        log.info("Adicionando supermercado: "+nombre);
        Proveedor proveedor= pp.adicionarProveedor(nombre,nit,fechaentrega,id);
        log.info("Adicionado supermercado: "+proveedor);
        return  proveedor;
    }
    public Cliente adicionarCliente(String nombre,String email , String id)
    {
        log.info("Adicionando supermercado: "+nombre);
        Cliente proveedor= pp.adicionarCliente(nombre,email,id);
        log.info("Adicionado supermercado: "+proveedor);
        return  proveedor;
    }
    public Cliente adicionarCliente(String nombre,String email,String nit,String direccion)
    {
        log.info("Adicionando supermercado: "+nombre);
        Cliente proveedor= pp.adicionarCliente(nombre,nit,direccion,email);
        log.info("Adicionado supermercado: "+proveedor);
        return  proveedor;
    }

    public Producto adicionarProducto(float cantidad,String codigoBarras,Date vencimiento,String marca,String nombre,float pesoEmpaque,String presentacion,float preciounidaddemedida,float preciounitario,String tipoproducto,String unidadMedida,float volumenempaque,float preciocompra, float precioventa,long estanteID,long bodegaID)
    {
        Producto producto=pp.adicionarProducto(cantidad,codigoBarras,vencimiento,marca,nombre,pesoEmpaque,presentacion,preciounidaddemedida,preciounitario,tipoproducto,unidadMedida,volumenempaque,preciocompra,precioventa,bodegaID,estanteID);
        return producto;
    }

}

