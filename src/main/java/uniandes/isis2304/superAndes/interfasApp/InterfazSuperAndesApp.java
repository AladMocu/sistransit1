/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Universidad	de	los	Andes	(Bogotá	- Colombia)
 * Departamento	de	Ingeniería	de	Sistemas	y	Computación
 * Licenciado	bajo	el	esquema	Academic Free License versión 2.1
 * 		
 * Curso: isis2304 - Sistemas Transaccionales
 * Proyecto: Parranderos Uniandes
 * @version 1.0
 * @author Germán Bravo
 * Julio de 2018
 * 
 * Revisado por: Claudia Jiménez, Christian Ariza
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package uniandes.isis2304.superAndes.interfasApp;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.HBox;
import org.apache.log4j.Logger;
import uniandes.isis2304.superAndes.negocio.*;

import javax.jdo.JDODataStoreException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.Method;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase principal de la interfaz
 * @author Germán Bravo
 * @Edition Albert Molano
 */
@SuppressWarnings("serial")

public class InterfazSuperAndesApp extends JFrame implements ActionListener
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(InterfazSuperAndesApp.class.getName());

	/**
	 * Ruta al archivo de configuración de la interfaz
	 */
	private static final String CONFIG_INTERFAZ = "./src/main/resources/config/interfaceConfigProy.json";
	
	/**
	 * Ruta al archivo de configuración de los nombres de tablas de la base de datos
	 */
	private static final String CONFIG_TABLAS = "./src/main/resources/config/TablasDB_P.json";
	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
    /**
     * Objeto JSON con los nombres de las tablas de la base de datos que se quieren utilizar
     */
    private JsonObject tableConfig;
    
    /**
     * Asociación a la clase principal del negocio.
     */
    private Proyecto proyecto;
    
	/* ****************************************************************
	 * 			Atributos de interfaz
	 *****************************************************************/
    /**
     * Objeto JSON con la configuración de interfaz de la app.
     */
    private JsonObject guiConfig;
    
    /**
     * Panel de despliegue de interacción para los requerimientos
     */
    private PanelDatos panelDatos;

    /**
     * Menú de la aplicación
     */
    private JMenuBar menuBar;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
    /**
     * Construye la ventana principal de la aplicación. <br>
     * <b>post:</b> Todos los componentes de la interfaz fueron inicializados.
     */
    public InterfazSuperAndesApp( )
    {
        // Carga la configuración de la interfaz desde un archivo JSON
        guiConfig = openConfig ("Interfaz", CONFIG_INTERFAZ);

        // Configura la apariencia del frame que contiene la interfaz gráfica
        configurarFrame ( );
        if (guiConfig != null)
        {
     	   crearMenu( guiConfig.getAsJsonArray("menuBar") );
        }

        tableConfig = openConfig ("Tablas BD", CONFIG_TABLAS);
        proyecto = new Proyecto (tableConfig);

    	String path = guiConfig.get("bannerPath").getAsString();
        panelDatos = new PanelDatos ( );

        setLayout (new BorderLayout());
        add (new JLabel (new ImageIcon (path)), BorderLayout.NORTH );
        add( panelDatos, BorderLayout.CENTER );
    }

	/* ****************************************************************
	 * 			Métodos de configuración de la interfaz
	 *****************************************************************/
    /**
     * Lee datos de configuración para la aplicació, a partir de un archivo JSON o con valores por defecto si hay errores.
     * @param tipo - El tipo de configuración deseada
     * @param archConfig - Archivo Json que contiene la configuración
     * @return Un objeto JSON con la configuración del tipo especificado
     * 			NULL si hay un error en el archivo.
     */
    private JsonObject openConfig (String tipo, String archConfig)
    {
    	JsonObject config = null;
		try
		{
			Gson gson = new Gson( );
			FileReader file = new FileReader (archConfig);
			JsonReader reader = new JsonReader ( file );
			config = gson.fromJson(reader, JsonObject.class);
			log.info ("Se encontró un archivo de configuración válido: " + tipo);
		}
		catch (Exception e)
		{
//			e.printStackTrace ();
			log.info ("NO se encontró un archivo de configuración válido");
			JOptionPane.showMessageDialog(null, "No se encontró un archivo de configuración de interfaz válido: " + tipo, "Parranderos App", JOptionPane.ERROR_MESSAGE);
		}
        return config;
    }

    /**
     * Método para configurar el frame principal de la aplicación
     */
    private void configurarFrame(  )
    {
    	int alto = 0;
    	int ancho = 0;
    	String titulo = "";

    	if ( guiConfig == null )
    	{
    		log.info ( "Se aplica configuración por defecto" );
			titulo = "Parranderos APP Default";
			alto = 300;
			ancho = 500;
    	}
    	else
    	{
			log.info ( "Se aplica configuración indicada en el archivo de configuración" );
    		titulo = guiConfig.get("title").getAsString();
			alto= guiConfig.get("frameH").getAsInt();
			ancho = guiConfig.get("frameW").getAsInt();
    	}

        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        setLocation (50,50);
        setResizable( true );
        setBackground( Color.WHITE );

        setTitle( titulo );
		setSize ( ancho, alto);
    }

    /**
     * Método para crear el menú de la aplicación con base em el objeto JSON leído
     * Genera una barra de menú y los menús con sus respectivas opciones
     * @param jsonMenu - Arreglo Json con los menùs deseados
     */
    private void crearMenu(  JsonArray jsonMenu )
    {
    	// Creación de la barra de menús
        menuBar = new JMenuBar();
        for (JsonElement men : jsonMenu)
        {
        	// Creación de cada uno de los menús
        	JsonObject jom = men.getAsJsonObject();

        	String menuTitle = jom.get("menuTitle").getAsString();
        	JsonArray opciones = jom.getAsJsonArray("options");

        	JMenu menu = new JMenu( menuTitle);

        	for (JsonElement op : opciones)
        	{
        		// Creación de cada una de las opciones del menú
        		JsonObject jo = op.getAsJsonObject();
        		String lb =   jo.get("label").getAsString();
        		String event = jo.get("event").getAsString();

        		JMenuItem mItem = new JMenuItem( lb );
        		mItem.addActionListener( this );
        		mItem.setActionCommand(event);

        		menu.add(mItem);
        	}
        	menuBar.add( menu );
        }
        setJMenuBar ( menuBar );
    }

	/* ****************************************************************
	 * 			CRUD de TipoBebida
	 *****************************************************************/
    /**
     * Adiciona un tipo de bebida con la información dada por el usuario
     * Se crea una nueva tupla de tipoBebida en la base de datos, si un tipo de bebida con ese nombre no existía
     */
    public void adicionarSupermercado( )
    {
    	try
    	{
    		String nombreTipo = JOptionPane.showInputDialog (this, "Nombre del supermercado", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE);
    		if (nombreTipo != null)
    		{
        		SuperAndes tb = proyecto.adicionarSupermercado (nombreTipo);
        		if (tb == null)
        		{
        			throw new Exception ("No se pudo crear un Supermercado con nombre: " + nombreTipo);
        		}
        		String resultado = "En adicionarSupermercado\n\n";
        		resultado += "Supermercado adicionado exitosamente: " + tb;
    			resultado += "\n Operación terminada";
    			panelDatos.actualizarInterfaz(resultado);
    		}
    		else
    		{
    			panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
    		}
		}
    	catch (Exception e)
    	{
//			e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
    }


    public void adicionarProveedor()
	{
        try
        {
            List<SuperAndes> a =proyecto.listarSupermercados();
            if(a.size()==0)
            {
                JOptionPane.showMessageDialog(this,"no hay supermercados creados","ERROR",JOptionPane.ERROR_MESSAGE);
                return;
            }
            String nombreProveedor = JOptionPane.showInputDialog (this, "Nombre del Proveedor", "Adicionar proveedor", JOptionPane.QUESTION_MESSAGE);
            String nit= JOptionPane.showInputDialog (this, "NIT del proveedor", "Adicionar proveedor", JOptionPane.QUESTION_MESSAGE);
            java.sql.Date fechaEntrega =new DatePickerSimple().getFecha();


            System.out.println(a.toArray());
            SuperAndes selection = (SuperAndes) JOptionPane.showInputDialog(this,
                    "Supermercado a añadir proveedor:",
                    "Proveedor",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    a.toArray(),null);

            long id= selection.getSuperandesID();


            if (nombreProveedor != null)
            {
                Proveedor tb = proyecto.adicionarProveedor (nombreProveedor,nit,fechaEntrega,id);
                if (tb == null)
                {
                    throw new Exception ("No se pudo crear un Proveedor con nombre: " + nombreProveedor);
                }
                String resultado = "En adicionar Proveedor\n\n";
                resultado += "Proveedor adicionado exitosamente: " + tb;
                resultado += "\n Operación terminada";
                panelDatos.actualizarInterfaz(resultado);
            }
            else
            {
                panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
            }
        }
        catch (Exception e)
        {
			e.printStackTrace();
            String resultado = generarMensajeError(e);
            panelDatos.actualizarInterfaz(resultado);
        }

	}

    public void adicionarProducto( )
    {
        try
        {

			List<Proveedor> a =proyecto.listarProveedores();

			List<Bodega> b=proyecto.listarBodegas();

			List <Estante> c=proyecto.listarEstantes();




			if(a.size()==0)
			{
				JOptionPane.showMessageDialog(this,"no hay proveedores creados","ERROR",JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(b.size()==0)
			{
				JOptionPane.showMessageDialog(this,"no hay Bodegas creadas","ERROR",JOptionPane.ERROR_MESSAGE);
				return;
			}
			if(c.size()==0)
			{
				JOptionPane.showMessageDialog(this,"no hay Estantes creados","ERROR",JOptionPane.ERROR_MESSAGE);
				return;
			}

            float cantidadProducto =Float.parseFloat( JOptionPane.showInputDialog (this, "Candidad de Producto", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE));
			String CodigodeBarras = JOptionPane.showInputDialog (this, "Codigo de barras", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE);
			Date fechaVencimiento =new DatePickerSimple().getFecha();
			String Marca = JOptionPane.showInputDialog (this, "Marca", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE);
			String Nombre = JOptionPane.showInputDialog (this, "Nombre del productoo", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE);
			float Pesoempaque = Float.parseFloat(JOptionPane.showInputDialog (this, "Peso del empaque", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE));
			String Precentacion = JOptionPane.showInputDialog (this, "Presentacion del producto", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE);
			float PrecioUnidadDeMedida = Float.parseFloat(JOptionPane.showInputDialog (this, "Precio por unidad de medida", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE));
			float PrecioUnitario = Float.parseFloat(JOptionPane.showInputDialog (this, "Precio unitatio", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE));


			eCategoria tipoProducto = (eCategoria) JOptionPane.showInputDialog(this,
					"Tipo Producto:",
					"Proveedor tipo producto",
					JOptionPane.QUESTION_MESSAGE,
					null,
					eCategoria.values(),null);


			String[] unidades={"gr","ml"};
			String unidadMedida =(String) JOptionPane.showInputDialog(this,
					"unidad de medida:",
					"Proveedor tipo producto",
					JOptionPane.QUESTION_MESSAGE,
					null,
					unidades,null);




			float Volumenempaque =Float.parseFloat( JOptionPane.showInputDialog (this, "Volumen empauque", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE));
			float PrecioVentaPublico =Float.parseFloat( JOptionPane.showInputDialog (this, "Precio Venta al publico", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE));

			float PrecioCompraSupermercado =Float.parseFloat( JOptionPane.showInputDialog (this, "Precio Compra al proveedor", "Adicionar supermercado", JOptionPane.QUESTION_MESSAGE));

            Proveedor idProveedor = (Proveedor) JOptionPane.showInputDialog(this,
                    "proveedor:",
                    "Proveedor",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    a.toArray(),null);
            Bodega idBodega = (Bodega) JOptionPane.showInputDialog(this,
                    "bodega:",
                    "Proveedor",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    b.toArray(),null);
            Estante idEstante = (Estante) JOptionPane.showInputDialog(this,
                    "Estante:",
                    "Proveedor",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    c.toArray(),null);
			Proveedor proveedor = (Proveedor) JOptionPane.showInputDialog(this,
					"Supermercado a añadir proveedor:",
					"Proveedor",
					JOptionPane.QUESTION_MESSAGE,
					null,
					a.toArray(),null);

			long sucursalID = idBodega.getSucursalID();

			JOptionPane.showMessageDialog(this,"hello there","yeape",JOptionPane.INFORMATION_MESSAGE);
			System.out.println("hello there :3");


            if (CodigodeBarras!=null&&fechaVencimiento!=null&& Marca!=null&&Nombre!=null&&Precentacion!=null)
            {
                Producto tb = proyecto.adicionarProducto(cantidadProducto,CodigodeBarras,fechaVencimiento,Marca,Nombre,Pesoempaque,Precentacion,PrecioUnidadDeMedida,PrecioUnitario,tipoProducto.toString(),unidadMedida,Volumenempaque,PrecioCompraSupermercado,PrecioVentaPublico,idEstante.getEstanteID(),idBodega.getBodegaID());

                if (tb == null)
                {
                    throw new Exception ("No se pudo crear un produnto");
                }
                String resultado = "En adicionar Proveedor\n\n";
                resultado += "Proveedor adicionado exitosamente: " + tb;
                resultado += "\n Operación terminada";
                panelDatos.actualizarInterfaz(resultado);
            }
            else
            {
                panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
            }


        }
        catch (Exception e)
        {
			e.printStackTrace();
            String resultado = generarMensajeError(e);
            panelDatos.actualizarInterfaz(resultado);
        }
    }


    public void adicionarCliente()
	{
		try
		{
			String nombreCliente = JOptionPane.showInputDialog (this, "Nombre del cliente", "Adicionar Cliete", JOptionPane.QUESTION_MESSAGE);
			String emailCliente = JOptionPane.showInputDialog (this, "Nombre del cliente", "Adicionar Cliete", JOptionPane.QUESTION_MESSAGE);




			String[] unidades={"persona Natural","empresa"};
			String unidadMedida =(String) JOptionPane.showInputDialog(this,
					"unidad de medida:",
					"Proveedor tipo producto",
					JOptionPane.QUESTION_MESSAGE,
					null,
					unidades,null);
			if(unidadMedida.equals("persona Natural")&&nombreCliente != null&&emailCliente!=null)
			{
				String DocumentoIdentificacion = JOptionPane.showInputDialog (this, "Documento de identificacion cliente", "Adicionar Cliete", JOptionPane.QUESTION_MESSAGE);


				Cliente tb = proyecto.adicionarCliente(nombreCliente,emailCliente,DocumentoIdentificacion);
				if (tb == null)
				{
					throw new Exception ("No se pudo crear un Cliente con nombre: " + nombreCliente);
				}
				String resultado = "En adicionarCliente\n\n";
				resultado += "Cliente adicionado exitosamente: " + tb;
				resultado += "\n Operación terminada";
				panelDatos.actualizarInterfaz(resultado);
			}
			else if (unidadMedida.equals("empresa")&&nombreCliente != null&&emailCliente!=null)
			{
				String Nit = JOptionPane.showInputDialog (this, "Nit cliente", "Adicionar Cliete", JOptionPane.QUESTION_MESSAGE);
				String Direccion = JOptionPane.showInputDialog (this, "Direccion cliente", "Adicionar Cliete", JOptionPane.QUESTION_MESSAGE);


				Cliente tb = proyecto.adicionarCliente(nombreCliente,Nit,Direccion,emailCliente);
				if (tb == null)
				{
					throw new Exception ("No se pudo crear un Cliente con nombre: " + nombreCliente);
				}
				String resultado = "En adicionarCliente\n\n";
				resultado += "Cliente adicionado exitosamente: " + tb;
				resultado += "\n Operación terminada";
				panelDatos.actualizarInterfaz(resultado);






			}
			else
			{
				panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
			}
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}
	}
	/* ****************************************************************
	 * 			Métodos administrativos
	 *****************************************************************/
	/**
	 * Muestra el log de Parranderos
	 */
	public void mostrarLogParranderos ()
	{
		mostrarArchivo ("SuperAndes.log");
	}

	/**
	 * Muestra el log de datanucleus
	 */
	public void mostrarLogDatanuecleus ()
	{
		mostrarArchivo ("datanucleus.log");
	}

	/**
	 * Limpia el contenido del log de parranderos
	 * Muestra en el panel de datos la traza de la ejecución
	 */
	public void limpiarLogParranderos ()
	{
		// Ejecución de la operación y recolección de los resultados
		boolean resp = limpiarArchivo ("parranderos.log");

		// Generación de la cadena de caracteres con la traza de la ejecución de la demo
		String resultado = "\n\n************ Limpiando el log de parranderos ************ \n";
		resultado += "Archivo " + (resp ? "limpiado exitosamente" : "NO PUDO ser limpiado !!");
		resultado += "\nLimpieza terminada";

		panelDatos.actualizarInterfaz(resultado);
	}

	/**
	 * Limpia el contenido del log de datanucleus
	 * Muestra en el panel de datos la traza de la ejecución
	 */
	public void limpiarLogDatanucleus ()
	{
		// Ejecución de la operación y recolección de los resultados
		boolean resp = limpiarArchivo ("datanucleus.log");

		// Generación de la cadena de caracteres con la traza de la ejecución de la demo
		String resultado = "\n\n************ Limpiando el log de datanucleus ************ \n";
		resultado += "Archivo " + (resp ? "limpiado exitosamente" : "NO PUDO ser limpiado !!");
		resultado += "\nLimpieza terminada";

		panelDatos.actualizarInterfaz(resultado);
	}

	/**
	 * Limpia todas las tuplas de todas las tablas de la base de datos de parranderos
	 * Muestra en el panel de datos el número de tuplas eliminadas de cada tabla
	 */
	public void limpiarBD ()
	{
		/*try
		{
    		// Ejecución de la demo y recolección de los resultados
			long eliminados [] = parranderos.limpiarParranderos();

			// Generación de la cadena de caracteres con la traza de la ejecución de la demo
			String resultado = "\n\n************ Limpiando la base de datos ************ \n";
			resultado += eliminados [0] + " Gustan eliminados\n";
			resultado += eliminados [1] + " Sirven eliminados\n";
			resultado += eliminados [2] + " Visitan eliminados\n";
			resultado += eliminados [3] + " Bebidas eliminadas\n";
			resultado += eliminados [4] + " Tipos de bebida eliminados\n";
			resultado += eliminados [5] + " Bebedores eliminados\n";
			resultado += eliminados [6] + " Bares eliminados\n";
			resultado += "\nLimpieza terminada";

			panelDatos.actualizarInterfaz(resultado);
		}
		catch (Exception e)
		{
//			e.printStackTrace();
			String resultado = generarMensajeError(e);
			panelDatos.actualizarInterfaz(resultado);
		}*/
	}

	/**
	 * Muestra la presentación general del proyecto
	 */
	public void mostrarPresentacionGeneral ()
	{
		mostrarArchivo ("data/00-ST-ParranderosJDO.pdf");
	}

	/**
	 * Muestra el modelo conceptual de Parranderos
	 */
	public void mostrarModeloConceptual ()
	{
		mostrarArchivo ("data/Modelo Conceptual Parranderos.pdf");
	}

	/**
	 * Muestra el esquema de la base de datos de Parranderos
	 */
	public void mostrarEsquemaBD ()
	{
		mostrarArchivo ("data/Esquema BD Parranderos.pdf");
	}

	/**
	 * Muestra el script de creación de la base de datos
	 */
	public void mostrarScriptBD ()
	{
		mostrarArchivo ("data/EsquemaParranderos.sql");
	}

	/**
	 * Muestra la arquitectura de referencia para Parranderos
	 */
	public void mostrarArqRef ()
	{
		mostrarArchivo ("data/ArquitecturaReferencia.pdf");
	}

	/**
	 * Muestra la documentación Javadoc del proyectp
	 */
	public void mostrarJavadoc ()
	{
		mostrarArchivo ("doc/index.html");
	}

	/**
     * Muestra la información acerca del desarrollo de esta apicación
     */
    public void acercaDe ()
    {
		String resultado = "\n\n ************************************\n\n";
		resultado += " * Universidad	de	los	Andes	(Bogotá	- Colombia)\n";
		resultado += " * Departamento	de	Ingeniería	de	Sistemas	y	Computación\n";
		resultado += " * Licenciado	bajo	el	esquema	Academic Free License versión 2.1\n";
		resultado += " * \n";
		resultado += " * Curso: isis2304 - Sistemas Transaccionales\n";
		resultado += " * Proyecto: Parranderos Uniandes\n";
		resultado += " * @version 1.0\n";
		resultado += " * @author Germán Bravo\n";
		resultado += " * Julio de 2018\n";
		resultado += " * \n";
		resultado += " * Revisado por: Claudia Jiménez, Christian Ariza\n";
		resultado += "\n ************************************\n\n";

		panelDatos.actualizarInterfaz(resultado);
    }


	/* ****************************************************************
	 * 			Métodos privados para la presentación de resultados y otras operaciones
	 *****************************************************************/


    /**
     * Genera una cadena de caracteres con la descripción de la excepcion e, haciendo énfasis en las excepcionsde JDO
     * @param e - La excepción recibida
     * @return La descripción de la excepción, cuando es javax.jdo.JDODataStoreException, "" de lo contrario
     */
	private String darDetalleException(Exception e)
	{
		String resp = "";
		if (e.getClass().getName().equals("javax.jdo.JDODataStoreException"))
		{
			JDODataStoreException je = (JDODataStoreException) e;
			return je.getNestedExceptions() [0].getMessage();
		}
		return resp;
	}

	/**
	 * Genera una cadena para indicar al usuario que hubo un error en la aplicación
	 * @param e - La excepción generada
	 * @return La cadena con la información de la excepción y detalles adicionales
	 */
	private String generarMensajeError(Exception e) 
	{
		String resultado = "************ Error en la ejecución\n";
		resultado += e.getLocalizedMessage() + ", " + darDetalleException(e);
		resultado += "\n\nRevise datanucleus.log y parranderos.log para más detalles";
		return resultado;
	}

	/**
	 * Limpia el contenido de un archivo dado su nombre
	 * @param nombreArchivo - El nombre del archivo que se quiere borrar
	 * @return true si se pudo limpiar
	 */
	private boolean limpiarArchivo(String nombreArchivo) 
	{
		BufferedWriter bw;
		try 
		{
			bw = new BufferedWriter(new FileWriter(new File (nombreArchivo)));
			bw.write ("");
			bw.close ();
			return true;
		} 
		catch (IOException e) 
		{
//			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Abre el archivo dado como parámetro con la aplicación por defecto del sistema
	 * @param nombreArchivo - El nombre del archivo que se quiere mostrar
	 */
	private void mostrarArchivo (String nombreArchivo)
	{
		try
		{
			Desktop.getDesktop().open(new File(nombreArchivo));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* ****************************************************************
	 * 			Métodos de la Interacción
	 *****************************************************************/
    /**
     * Método para la ejecución de los eventos que enlazan el menú con los métodos de negocio
     * Invoca al método correspondiente según el evento recibido
     * @param pEvento - El evento del usuario
     */
    @Override
	public void actionPerformed(ActionEvent pEvento)
	{
		String evento = pEvento.getActionCommand( );		
        try 
        {
			Method req = InterfazSuperAndesApp.class.getMethod ( evento );
			req.invoke ( this );
		} 
        catch (Exception e) 
        {
			e.printStackTrace();
		} 
	}
    
	/* ****************************************************************
	 * 			Programa principal
	 *****************************************************************/
    /**
     * Este método ejecuta la aplicación, creando una nueva interfaz
     * @param args Arreglo de argumentos que se recibe por línea de comandos
     */
    public static void main( String[] args )
    {
        try
        {
        	
            // Unifica la interfaz para Mac y para Windows.
            UIManager.setLookAndFeel( UIManager.getCrossPlatformLookAndFeelClassName( ) );
            InterfazSuperAndesApp interfaz = new InterfazSuperAndesApp( );
            interfaz.setVisible( true );
        }
        catch( Exception e )
        {
            e.printStackTrace( );
        }
    }
}
